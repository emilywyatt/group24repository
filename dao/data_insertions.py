
import mysql.connector
from datetime import datetime
import re
'''
    This fucntion inserts jsons into the database
'''
def insert_data(json_object):
    #converts the given  time into a python datetime object
    date = json_object['time']
    x = re.search('[.]', date)
    datetime_object = date[:x.start()]
    datetime_object = datetime.strptime(datetime_object, '%d-%b-%Y (%H:%M:%S')
    ############################################################
    #forms connection with mysql database
    cnx = mysql.connector.connect(host = '192.168.99.100',
                                  user='root', password='ppp',
                                  database = 'deal_data')
    cursor = cnx.cursor(buffered=True)

    ##################################################
    #queries intrument table for the correct instument id of the given instrument
    name = json_object['instrumentName']
    query = ("SELECT instrument_id FROM instrument WHERE instrument_name= \'{}\'".format(name))
    cursor.execute(query)

    #returns the correct instrument id
    for (instrument_id) in cursor:
        inst_id = instrument_id[0]
    ###################################################################
    # queries counter party table for currect counter party id
    query = ("SELECT cp_id FROM counter_party WHERE cp_name =\'{}\'".format(json_object['cpty']))
    cursor.execute(query)
    #retunr the correct counter party id
    for (cp_id) in cursor:
      cp_id = cp_id[0]
    ##################################################################

    # returns the current length of the deal table
    query = ("SELECT COUNT(*) FROM deal")
    cursor.execute(query)
    for count in cursor:
        deal_id = count[0] + 1 #calculates the next id
    #form insertion query to insert trade into deal table
    add_deal = ("INSERT INTO deal "
                "VALUES (%s, %s, %s, %s, %s, %s, %s)")

    #calculate price, type and quantity
    price = int(round(float(json_object['price'])*100,0))
    type = json_object['type']
    quantity = int(json_object['quantity'])

    #add json data into deal database
    data_deal = (deal_id,inst_id,cp_id,price,type,quantity,datetime_object)
    cursor.execute(add_deal, data_deal)

    #commit changes to the database and close connection
    cnx.commit()
    cursor.close()
    cnx.close()

'''
    edit time and edit price
'''
