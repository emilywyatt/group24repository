from sseclient import SSEClient
import json
from data_insertions import insert_data
from flask import Flask, Response
from flask_cors import CORS

# messages = SSEClient('http://localhost:8080/datastream')

# for msg in messages:
#
#     outputMsg = msg.data
#     outputJS = json.loads(outputMsg)
#     # FilterName = "data"
#     # print(outputJS)
#     insert_data(outputJS)
#
app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return "App is running"


@app.route('/deals/stream')
def stream():
    # messages = SSEClient('http://localhost:8080/datastream')
    def eventStream():
        messages = SSEClient('http://localhost:8080/datastream')

        for msg in messages:

            outputMsg = msg.data
            outputJS = json.loads(outputMsg)
    # FilterName = "data"
            print(outputJS)
            insert_data(outputJS)

            # wait for source data to be available, then push it
            yield str(outputJS)

    return Response(eventStream(), mimetype="text/json")

if __name__ == "__main__":
    print ('hi')
    app.run(port=5000, threaded=True, host=('0.0.0.0'))