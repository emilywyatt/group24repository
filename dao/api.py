import mysql.connector
import flask
from flask import request, jsonify
# from ...Python_Deal_Code import report_queries, report_functions
from datetime import datetime


app = flask.Flask(__name__)
app.config["DEBUG"] = True



@app.route('/', methods=['GET'])
def home():
    return "<h1>Deals archive</h1><p>This site is a prototype API.</p>"


def converttojson(data):
    data2 = []
    listofHeaders = ['deal_id', 'instrument_id', 'cp_id', 'price', 'type', 'quantity', 'time']
    big_json = []
    for tuple in data:
        lst = list(tuple)
        lst[6] = str(tuple[6])
        data2.append(lst)
        zipbObj = zip(listofHeaders, lst)
        dictObj = dict(zipbObj)
        big_json.append(dictObj)
    deals = big_json
    return deals


def access_deals():
    cnx = mysql.connector.connect(host='192.168.99.100',
                                  user='root', password='ppp',
                                  database='deal_data')
    cursor = cnx.cursor(buffered=True)
    query = "SELECT deal_id, instrument_id, cp_id, price, type, quantity, time FROM deal"
    cursor.execute(query)
    data = cursor.fetchall()
    return data


@app.route('/database-status/', methods=['GET'])
def check_status():
    try:
        cnx = mysql.connector.connect(host='192.168.99.100',
                                      user='root', password='ppp',
                                      database='deal_data')
    except mysql.connector.errors.InterfaceError:
        connected = [{'databaseStatus':'down'}]
        return jsonify(connected)

    connected = [{'databaseStatus' : 'up'}]
    return jsonify(connected)
@app.route('/deals/all/', methods=['GET'])
def get_deals():
    data = access_deals()
    json = converttojson(data)
    return jsonify(json)

# mysql.connector.errors.InterfaceError

@app.route('/deals/', methods=['GET'])
def api_id():
    if 'deal_id' in request.args:
        id = int(request.args['deal_id'])
        string = 'deal_id'
    elif 'cp_id' in request.args:
        id = int(request.args['cp_id'])
        string = 'cp_id'
    elif 'instrument_id' in request.args:
        id = int(request.args['instrument_id'])
        string = 'instrument_id'
    elif 'price' in request.args:
        id = int(request.args['price'])
        string = 'price'
    elif 'quantity' in request.args:
        id = int(request.args['quantity'])
        string = 'quantity'
    elif 'time' in request.args:
        id = str(request.args['time'])
        string = 'time'
    elif 'type' in request.args:
        id = str(request.args['type'])
        string = 'type'
    else:
        return "Error: No id field provided. Please specify an id."
    results = []
    data = access_deals()
    json = converttojson(data)
    for deal in json:
        if deal[string] == id:
            results.append(deal)
    return jsonify(results)

if __name__ == "__main__":
    app.run(debug=True)


