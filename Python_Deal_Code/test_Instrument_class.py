import pytest
from Instrument import *


# arrange
Name = 'test'
variance = 1
basePrice = 0
drift = 1

# act
test_instrument = Instrument(Name,basePrice,drift,variance)

def test_checkname():
        
    # assert
    assert test_instrument.name == Name

def test_checkvariance():

    # assert
    assert test_instrument.getVariance() == variance

def test_checkbaseprice():

    # assert
    assert test_instrument.getPrice() == basePrice

def test_checkdrift():

    # assert
    assert test_instrument.getDrift() == drift

