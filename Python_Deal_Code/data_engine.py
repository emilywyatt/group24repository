import time
from flask import Flask, Response
from flask_cors import CORS
import time
import numpy, random
from datetime import datetime, timedelta
import json
from Instrument import Instrument
from deal_code import instruments, counterparties, NUMBER_OF_RANDOM_DEALS, TIME_PERIOD_MILLIS, EPOCH, RandomDealData


# class ThreadedEventChannel(EventChannel):
#     def __init__(self, blocking=True):
#         self.blocking = blocking
#         super(ThreadedEventChannel, self).__init__()

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return "App is running"

@app.route('/datastream')
def stream():
    InstrList = RandomDealData.createInstrumentList()
    def eventStream():
        while True:
            # wait for source data to be available, then push it
            time.sleep(1)
            yield "data:" + RandomDealData.createRandomData(InstrList)+"\n\n"
            
    return Response(eventStream(), mimetype="text/json")

 # def publish(self, event, *args, **kwargs):
 #        threads = []
 #        if event in self.subscribers.keys():
 #            for callback in self.subscribers[event]:
 #                threads.append(threading.Thread(
 #                  target=callback,
 #                  args=args,
 #                  kwargs=kwargs
 #                ))
 #            for th in threads:
 #                th.start()
 #
 #            if self.blocking:
 #                for th in threads:
 #                    th.join()

def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))
