from datetime import datetime,timedelta
import report_queries

debug = False

def calculate_avg_buy_and_sell_price(price_list):
    return int(sum(price_list)/len(price_list))

def calculate_end_position(list_of_deals, starting_position=0):
    end_position = starting_position
    for deal in list_of_deals:
        #if debug == True: print(deal)
        if deal[1] == 'B':
            end_position = end_position-deal[0]*deal[2]
        elif deal[1] == 'S':
            end_position = end_position+deal[0]*deal[2]
        else:
            raise Exception('Direction of deal invalid (must be B or S)')

    return end_position

def next_day(date):
    return date + timedelta(seconds=60)

#print(next_day(datetime(2012, 6, 18, 10, 34, 9)))

def calculate_all_end_positions(initial_date,last_date):
    end_positions = [0]
    starting_position = 0
    start_date = initial_date
    end_date = next_day(start_date)
    while end_date < last_date:
        records = report_queries.query_database_end_positions(start_date,end_date)
        print(records)
        end_position = calculate_end_position(records,starting_position)
        end_positions.append(end_position)
        starting_position = end_position
        #print(start_date,end_date, end_position)
        start_date = next_day(start_date)
        end_date = next_day(end_date)
    return end_positions
#print(next_day('2019-08-12 15:28:35'))
#print(calculate_all_end_positions(datetime(2019, 8, 12, 15, 28, 35),datetime(2019, 8, 12, 15, 33, 55)  ))
  
def profit_calculated(instr_name):
    Buys = report_queries.query_database_for_prices('B', instr_name)
    Sells = report_queries.query_database_for_prices('S', instr_name)
    Buys_list = [list(elem) for elem in Buys]
    Sells_list = [list(elem) for elem in Sells]
    #print(Buys_list)
    #print(Sells_list)
    Salenumber = len(Sells) #How long the list is
    Iterator = 0 #To move through the Sales list
    profit = 0
    if Iterator < Salenumber:
        for Buy in Buys_list:
            quantity = Buy[1] # Look at quantity
            while quantity != 0 and Iterator < Salenumber: # If you sold more than you bought
                if quantity <= Sells_list[Iterator][1]:
                    profit += quantity * (Sells_list[Iterator][0]-Buy[0])
                    Sells_list[Iterator][1] -= quantity
                    Buy[1] = 0
                    quantity = 0
                    #print(Buys_list)
                    #print(Sells_list)
                else:
                    profit += Sells_list[Iterator][1] * (Sells_list[Iterator][0]-Buy[0]) # If you sold less than you bought
                    #print(profit,Buy[0])
                    quantity -= Sells_list[Iterator][1]
                    Sells_list[Iterator][1] = 0
                    Iterator += 1 # move to next sell
                    #print(Buys_list)
                    #print(Sells_list)
    realised_profit = profit
    if Buys_list:
        FinalBuyPrice = Buys_list[-1][0]
        #FirstBuyPrice = Buys_list[0][0]
    else:
        FinalBuyPrice = 0
        #FirstBuyPrice = 0
    if Sells_list:    
        FinalSellPrice = Sells_list[-1][0]
        #FirstSellPrice = Sells_list[0][0]
    else:
        FinalSellPrice = 0
        #FirstSellPrice = 0
    if Iterator < Salenumber: #Short postion 
        open_position = sum([el[1]*(el[0]-FinalBuyPrice) for el in Sells_list])
        #print(Buys_list)
    else: #Long Position
        open_position = sum([el[1]*(FinalSellPrice - el[0]) for el in Buys_list])
    effective_profit = open_position + realised_profit

    return (realised_profit,effective_profit)

print(profit_calculated('Interstella'))


        

