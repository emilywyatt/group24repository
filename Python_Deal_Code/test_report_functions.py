import pytest
from report_functions import *

def test_returns_avg_of_a_list():
    #Arrange
    test_list = [1,2,3,4,5]

    #Act
    test_average = calculate_avg_buy_and_sell_price(test_list)

    #Assert
    assert test_average == 3

def test_returns_correct_end_position():
    #Arrange
    test_tuple_list = [(2,'B'), (3,'S'),(4,'B'), (6, 'S')]

    #Act
    test_end_position = calculate_end_position(test_tuple_list)

    #Assert 
    assert test_end_position == 3