import time
import numpy, random
from datetime import datetime, timedelta
import json
from Instrument import *
import pytest
from deal_code import *

def test_check_instrument_list_method_returns_list_of_instrument_objects():
    #arrange
    
    #act
    InstrumentList = RandomDealData.createInstrumentList()

    #assert
    assert type(InstrumentList) == list and isinstance(InstrumentList[0],Instrument)

def test_check_create_random_data_returns_json():
    #arrange
    InstrumentList = RandomDealData.createInstrumentList()

    #act
    random_data = RandomDealData.createRandomData(InstrumentList)

    #assert
    
    def json_validator(data):
        try:
            json.loads(data)
            return True
        except ValueError as error:
            #print("invalid json: %s" % error)
            return False

    assert json_validator(random_data) == True

