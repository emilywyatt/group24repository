import pytest
from Instrument import *

def test_check_method_works():
    #arrange
    Name = 'test'
    variance = 0
    basePrice = 0
    drift = 0
    test_instrument = Instrument(Name,basePrice,drift,variance)
    direction = 'B'

    #act
    next_price = test_instrument.calculateNextPrice(direction)

    #assert

    assert next_price == 0