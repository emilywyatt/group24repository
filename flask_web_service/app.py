from flask import Flask, Response, json
from flask_cors import CORS
import sseclient
app = Flask(__name__)
CORS(app)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/database_server_status')
def get_database_status():
    data = {
        'database_server_status': 'offline'
    }
    data_as_json = json.dumps(data)
    return Response(
        data_as_json,
        mimetype='application/json',
        status=200)

@app.route('/data_generated')
def stream():
    client = sseclient.SSEClient('http://localhost:8080/datastream')
    def get_data_from_engine():
        for event in client:
            yield event.data
    return Response(get_data_from_engine(), mimetype="text/json")


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
